/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var passport = require('passport');

module.exports = {	


  /**
   * `AuthController.login()`
   */
  login: function (req, res, next) {
      passport.authenticate('local', function(err, user, info) {

        if (err) { return next(err); }

        // If 
        if (!user) { 
            req.addFlash('bad_user', 'Bad user'); 
            return res.redirect('/login'); 
        }

        req.logIn(user, function(err) {
          if (err) { return next(err); }
          return res.redirect('/' + user.name);
        });

      })(req, res, next);
  },


  /**
   * `AuthController.new()`
   */
  new: function (req, res) {
    if (req.body)    
    {
        User.create({
          'name' : req.body.name,
          'surname' : req.body.surname,
          'email' : req.body.email,
          'password' : req.body.password,

        }).exec(function(err, user){          
          if ( !err ) res.redirect('/');
        });

    }
    else res.redirect('/register');
  },


  logout: function (req,res){
    req.logout();
    res.redirect('/');
  }


};

