/**
 * CalendarController
 *
 * @description :: Server-side logic for managing calendars
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	


  /**
   * `CalendarController.show()`
   */
  show: function (req, res) {
    res.view('calendar/calendar');
  },


   /**
   * `CalendarController.showAll()`
   */
  showAll: function (req, res) {
    res.view('calendar/calendars');
  },


  /**
   * `CalendarController.new()`
   */
  new: function (req, res) {
    return res.json({
      todo: 'new() is not implemented yet!'
    });
  },

   /**
   * `CalendarController.update()`
   */
  update: function (req, res) {
    return res.json({
      todo: 'update() is not implemented yet!'
    });
  }

};

