/**
 * WallController
 *
 * @description :: Server-side logic for managing muroes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	


  /**
   * `WallController.index()`
   */
  index: function (req, res) {
    Food.find()
        .populate('tags')
        .populate('id_user')
        .where({'publish' : 1})
        .exec(function(err, foods){
            if (err) res.notFound();
            var inLog = req.user || false;
            return res.render('wall/index', { 'foods': foods, isLogging : inLog });
        }); 
  },

   /**
   * `WallController.publish()`
   */
  publish: function (req, res) {
      if (req.body.publish_id)
      {
          Food.update({id: req.body.publish_id}, {publish: 1})
              .exec(function(err, food){
                  if (err) res.json(500, {error: 'not ok'});
                  res.json(200, {message : 'ok'});
              });
      }
  }
};

