/**
 * FriendsController
 *
 * @description :: Server-side logic for managing friends
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	


  /**
   * `FriendsController.show()`
   */
  show: function (req, res) {
    console.log(sails.config.globals);
    res.view('friends/show', {layout : 'layout'});
  },

   /**
   * `FriendsController.follow()`
   */
  follow: function (req, res) {
    return res.json({
      todo: 'follow() is not implemented yet!'
    });
  },

   /**
   * `FriendsController.unfollow()`
   */
  unfollow: function (req, res) {
    return res.json({
      todo: 'unfollow() is not implemented yet!'
    });
  }

};

