/**
 * FoodController
 *
 * @description :: Server-side logic for managing Foods
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
/**
   * `FoodController.new()`
   */
  new: function (req, res) {
     res.view('food/new', {
          'food' : false
        });
  },


  /**
   * `FoodController.edit()`
   */
  edit: function (req, res) {
      var id = req.param('id');
      Food.findOne(id)
        .populate('tags')
        .exec(function(err, fd){
        var food = fd || false;
        console.log(food);
        res.view('food/new', {
          'food' : food
        });
     });
  },

  /**
   * `FoodController.show()`
   */
  save: function (req, res) {
    if (req.body)
    {
      var x = req.body;

       Food.create({
          title : x.title,
          description : x.description,
          id_user : req.user.id
       }).exec(function(err, food){

          req.file('image').upload({
            // don't allow the total upload size to exceed ~10MB
            maxBytes: 10000000
          },function whenDone(err, uploadedFiles) {
            if (err) {
              return res.negotiate(err);
            }

            if (uploadedFiles.length !== 0){

                // Save the "fd" and the url where the avatar for a user can be accessed
                Food.update(food.id, {

                  // Generate a unique URL where the avatar can be downloaded.
                  thumbnailURL: require('util').format('%s/food/avatar/%s',sails.getBaseUrl(), food.id),
                   // Grab the first file and use it's `fd` (file descriptor)
                  avatarFd: uploadedFiles[0].fd
                })
                .exec(function (err){
                  if (err) return res.negotiate(err);
                  //return res.ok();
                });
            }

            // Save tags after destroy
            if (x['tags_list[]']){
              Tag_food.destroy({id_food: food.id})
                        .exec(function(err, tags){
                          if (err) return;
                          var array_data = new Array();
                          for (i=0; i< x['tags_list'].length; i++){
                              array_data.push({
                                id_food : food.id,
                                label : x['tags_list[]'][i]
                              }); 
                          }
                          /*for (aux in x['tags_list[]']){
                              array_data.push({
                                id_food : food.id,
                                label : x['tags_list[]'][aux]
                              }); 
                          }*/
                          Tag_food.create(array_data)
                                  .exec(function(err, tags){
                                    if (err) console.log('Not tags');
                                  });
                        });
                
            }


            res.redirect('/food/' + food.id);

          });
       });
    }
  },


  /**
   * `FoodController.avatar()`
   */
  avatar: function (req, res){

    Food.findOne(req.param('id')).exec(function (err, food){
      if (err) return res.negotiate(err);
      if (!food) return res.notFound();

      // User has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!food.thumbnailURL) {
        return res.notFound();
      }

      var SkipperDisk = require('skipper-disk');
      var fileAdapter = SkipperDisk(/* optional opts */);

      // Stream the file down
      fileAdapter.read(food.avatarFd)
      .on('error', function (err){
        return res.serverError(err);
      })
      .pipe(res);
    });
  },

  /**
   * `FoodController.show()`
   */
  show: function (req, res) {
    return res.json({
      todo: 'show() is not implemented yet!'
    });
  },

  /**
   * `FoodController.show()`
   */
  showAll: function (req, res) {
    Food.find()
        .populate('tags')
        .exec(function(err, fds){
        var foods = fds || false;
        console.log(foods);
        res.view('food/foods', {
          'foods' : foods
        });
    });
  },


  /**
   * `FoodController.delete()`
   */
  delete: function (req, res) {
      Food.destroy({id : req.param('id')})
        .exec(function(err, fd){
           if (err) res.json(500, {error: 'not ok'});
            res.json(200, {message : 'ok'});
     });
  },

};

