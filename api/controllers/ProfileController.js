/**
 * ProfileController
 *
 * @description :: Server-side logic for managing profiles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var bcrypt = require('bcrypt');
module.exports = {
	

  /**
   * `ProfileController.dashboard()`
   */
  dashboard: function (req, res) {
    res.view('profile/dashboard');
  },


  /**
   * `ProfileController.index()`
   */
  index: function (req, res) {
      User.findOne(req.user.id).exec(function(err, user){
        var usuario = user || false;
        res.view('profile/edit', {
          'user' : usuario
        });
     });
  },



  /**
   * `ProfileController.save()`
   */
  save: function (req, res) {
    if ( req.body ){

        var new_data = { name: req.body.name,
                          surname: req.body.surname,
                          email: req.body.email,                    
                        };
        
        if ( req.body.password != ''  && req.body.password === req.body.confirm){            
           new_data.password = bcrypt.hashSync(req.body.password, 10);
        }

      
       User.update(
                { id: req.user.id }, 
                new_data )
         .exec(function(err, users) {
            res.json(200, { message: 'ok' });
         }); 

    }
  }
};

