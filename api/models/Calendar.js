module.exports = {
	attributes : {
		id_user : {
			model : 'user'
		},
		month : {
			type : 'integer'
		},
		year : {
			type : 'integer'
		},
		foods : {
			collection : 'food',
			via : 'id_food',
			through : 'calendar_food'
		},
		
		
	}
}