module.exports = {
	attributes : {
		id_calendar : {
			model : 'calendar'
		},
		id_food : {
			model : 'food'
		},
		day : {
			type : 'integer'
		}
	}
}