/**
 * Food.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
	title : {
		type : 'string'
	},
	description : {
		type : 'text'
	},
	thumbnailURL : {
		type : 'string'
	},
	avatarFd : {
		type : 'text'
	},
	/*id_user : {
		type: 'integer'
	//	model : 'user',
	},*/
	publish : {
		type : 'integer'
	},
	num_likes : {
		type : 'integer'
	},

	 // Add a reference to User
    id_user: {
      model: 'user'
    },

    /*calendars : {
    	collection : 'calendar_food',
    	via : 'id_food'
    },*/

    calendars : {
		collection : 'calendar',
		via : 'id_calendar',
		through : 'calendar_food'
	},

	tags : {
		collection : 'tag_food',
		via : 'id_food'
	}

  }
};

