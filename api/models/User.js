
var bcrypt = require('bcrypt');

module.exports = {
	attributes : {
		name : {
			type : 'string'
		},
		surname : {
			type : 'string'
		},
		email : {
			type : 'string'
		},
		password : {
			type : 'string'
		},
		createdAt : {
			type : 'datetime'
		},
		updatedAt : {
			type : 'datetime'
		},
		

		// Add reference to Food
		foods: {
			collection: 'food',
			via : 'id_user'
		},

		calendars : {
			collection : 'calendar',
			via : 'id_user'
		},

		friends : {
			collection : 'friends',
			via : 'id_user'
		}
	},

	beforeCreate: function(user, cb) {
	    bcrypt.genSalt(10, function(err, salt) {
	      bcrypt.hash(user.password, salt, function(err, hash) {
	        if (err) {
	          console.log(err);
	          cb(err);
	        }else{
	          user.password = hash;
	          cb(null, user);
	        }
	      });
	    });
	}
}