module.exports = function(req, res, next){
	if ( req.user ) res.redirect('/' + req.user.name);
	else next();
}