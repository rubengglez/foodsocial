
    (function($){
		
		// Profile
		$('#js-send-profile').on('click', function(e){
			e.preventDefault();
			$.ajax({
				url : '/profile',
				method : 'PUT',
				contentType : 'application/json',
				data: JSON.stringify({
					name : $('input[name="name"]').val(),
					surname : $('input[name="surname"]').val(),
					email : $('input[name="email"]').val(),
					password : $('input[name="password"]').val(),
					confirm : $('input[name="confirm"]').val(),
				})
			})
			.done(function() {
			   	$('.alert.alert-success').removeClass('hidden');
			   	setTimeout(function(){
			   		$('.alert.alert-success').addClass('hidden');
			   	}, 3000);
			})
			.fail(function() {
			   	$('.alert.alert-danger').removeClass('hidden');
			   	setTimeout(function(){
			   		$('.alert.alert-danger').addClass('hidden');
			   	}, 3000);
			});
		});

		// New Food
		$('#tag').on('keydown', function(event){
			var x = event.which || event.keyCode;
			var obj = $(this);
			var tags = $('#tags-list').val();

			// Tape ','
			if (x === 188)
			{
				$('#resume-tags p').append('<span class="label tag-item label-default" style="">' + $(this).val() + '</span><input type="hidden" name="tags_list[]" value="' + $(this).val() + '" >');
				$(this).val('');
			}

		});

		// Edit Food
		$('#eliminar').on('click', function(e){
			$('.img-figure').remove();
			$('#resume-tags p').append('<label class="control-label" for="exampleInputFile">Photo of the food</label><input type="file" name="image" id="exampleInputFile">');
			$(this).remove();
		});


		// Publish Food
		$('#publish').on('click', function(e){
			e.preventDefault();
			$.ajax({
				url : '/publish',
				method : 'PUT',
				contentType : 'application/json',
				data: JSON.stringify({
					publish_id : $(this).data('id'),
				})
			})
			.done(function() {
			   	$('.alert.alert-success').removeClass('hidden');
			   	setTimeout(function(){
			   		$('.alert.alert-success').addClass('hidden');
			   	}, 3000);
			})
			.fail(function() {
			   	$('.alert.alert-danger').removeClass('hidden');
			   	setTimeout(function(){
			   		$('.alert.alert-danger').addClass('hidden');
			   	}, 3000);
			});
		});


		// Foods
		$('.js-delete').on('click', function(e){
			e.preventDefault();
			$.ajax({
				url : '/food/' + $(this).data('id'),
				method : 'DELETE',
				contentType : 'application/json'
			})
			.always(function() {
			    window.location.reload();
			});
		});

    })(jQuery);