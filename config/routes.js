/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

 /* '/': {
    view: 'homepage', 
    locals : {
      layout : 'layout_out'
    }
  },*/

  'get /' : 'Index.index',

  'post /login': 'AuthController.login',
  'get /logout': 'AuthController.logout',
  'get /register': {
      view : 'register',
      locals : {
        layout : 'layout_out'
      }
  },
  'post /newuser': 'AuthController.new',

  'get /wall' : 'WallController.index',
  'put /publish' : 'WallController.publish',

  'get /profile' : 'Profile.index',
  'put /profile' : 'Profile.save',

  'get /food' : 'Food.new',
  'get /food/:id' : 'Food.edit',
  'get /food/avatar/:id' : 'Food.avatar',
  'post /food' : 'Food.save',
  'get /viewfood' : 'Food.show',
  'get /foodAll' : 'Food.showAll',
  'delete /food/:id' : 'Food.delete',

  'get /showCalendars' : 'Calendar.showAll',
  'put /updateCalendar/:id' : 'Calendar.update',
  'get /calendar/month/:id' : 'Calendar.show',
  'get /newcalendar' : 'Calendar.new',

  'get /friends' : 'Friends.show',
  'post /follow' : 'Friends.follow',
  'delete /unfollow' : 'Friends.unfollow',

  'get /dame' : 'Caca.merda',

  'GET /api/user': {blueprint: 'find', model: 'user'},
  'POST /api/user': {blueprint: 'find', model: 'user'},

  'get /:name' : 'Profile.dashboard',

 /* 'post /login': {
    view: '/privacy',
    locals: {
      layout: 'users'
    }
  },*/

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
