module.exports = {
	http : {
		isRealtime : function(res, req, next)
		{
			sails.config.globals.isRealtime = false;
			if (req.path === 'friends'){
				sails.config.globals.isRealtime = true;
			}
			next();
		}
	}
}